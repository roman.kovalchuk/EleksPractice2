﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EleksPractice2
{
    static class MatrixCalculator
    {
        private static int length = 8;
        private static int[,] matrix;

        public static void Print()
        {
            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        public static void FindFirstColWithoutNegativ()
        {

            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    if (matrix[j, i] <= 0)
                    {
                        break;
                    }
                    else if (matrix[j, i] <= 0 && j == length - 1)
                    {
                        Console.WriteLine($"There is all positive at {i} col");
                        return;
                    }
                }
            }
            Console.WriteLine("There isnt any col with all positive");
        }

        public static void SortBySameElements()
        {
            int[] suchElementsCounts = new int[length];
            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    for (int o = j + 1; o < length; o++)
                    {
                        if (matrix[i, j] == matrix[i, o])
                        {
                            suchElementsCounts[i]++;
                        }
                    }
                }
            }

            for (int i = 0; i < length; i++)
            {
                for (int h = 0; h < length - 1; h++)
                {
                    if (suchElementsCounts[h] > suchElementsCounts[h + 1])
                    {
                        for (int o = 0; o < length; o++)
                        {
                            int temporary = matrix[h, o];
                            matrix[h, o] = matrix[h + 1, o];
                            matrix[h + 1, o] = temporary;

                        }
                        int temp = suchElementsCounts[h + 1];
                        suchElementsCounts[h + 1] = suchElementsCounts[h];
                        suchElementsCounts[h] = temp;
                    }
                }

            }

        }

        public static void FindSumRowWithNegative()
        {
            List<int> sums = new List<int>();
            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    if (matrix[i, j] < 0)
                    {
                        List<int> row = new List<int>();
                        for (int g = 0; g < length; g++)
                        {
                            row.Add(matrix[i, g]);
                        }
                        Console.WriteLine($"Sum: {row.Sum()}");
                        break;
                    }
                }
            }
        }

        public static void FindNumberWhichIsMaxInColAndMinInRow()
        {
            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    List<int> row = new List<int>();
                    List<int> col = new List<int>();
                    for (int q = 0; q < length; q++)
                    {
                        row.Add(matrix[i, q]);
                        col.Add(matrix[q, j]);
                    }
                    if (row.Min() == matrix[i, j] && col.Max() == matrix[i, j])
                    {
                        Console.WriteLine($"Col: {j} . Row: {i}.");
                    }
                }
            }
        }

        public static void FindColsSameAsRows()
        {
            for (int i = 0; i < length; i++)
            {
                List<int> row = new List<int>();
                List<int> col = new List<int>();
                for (int j = 0; j < length; j++)
                {
                    row.Add(matrix[i, j]);
                    col.Add(matrix[j, i]);
                }
                if (row.Equals(col))
                {
                    Console.WriteLine(i);
                }
            }
        }

        static MatrixCalculator()
        {
            matrix = new int[length, length];
            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    matrix[i, j] = (i - 5) * (j - 3);
                }
            }
        }
    }
}
