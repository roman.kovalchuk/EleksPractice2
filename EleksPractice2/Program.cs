﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EleksPractice2
{
    static class Program
    {
        static void Main()
        {
            MatrixCalculator.Print();
            MatrixCalculator.FindColsSameAsRows();
            Console.WriteLine();
            MatrixCalculator.FindFirstColWithoutNegativ();
            Console.WriteLine();
            MatrixCalculator.FindSumRowWithNegative();
            Console.WriteLine();
            MatrixCalculator.FindNumberWhichIsMaxInColAndMinInRow();
            Console.WriteLine();
            MatrixCalculator.SortBySameElements();
            MatrixCalculator.Print();
            Console.ReadLine();
        }
    }
}
